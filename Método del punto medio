# Método del punto medio

import numpy as np
from matplotlib import pyplot as plt
from matplotlib.widgets import Slider, Button
from math import sqrt


# SISTEMA DE EDOs
def s(t, st, it):       # ds/dt = S(s, i, t)
    return -0.5*st*it     
def i(t, st, it):               # di/dt = I(s, i, t)
    return 0.5*st*it-(1/3)*it       
def r(t, st, it, rt):           # dr/dt = R(i, t)
    return (1/3)*it


N = 50
a, b = 0,150 # intervalo de integración
h = (b-a)/N

# Condiciones iniciales
s_0 = 1
i_0 = 1.27*10**(-6)
r_0 = 0

ts = np.linspace(a, b, N+1)
us = [s_0]
ui = [i_0]
ur = [r_0]


# kas para s
def ks1(t,st,it):
    return s(t,st,it)
    
def ks2(t,st,it):
    return s(t+(h/2),st+(h/2)*ks1(t,st,it),it)

# kas para i
def ki1(t,st,it):
    return i(t,st,it)
    
def ki2(t,st,it):
    return i(t+(h/2),st+(h/2)*ks1(t,st,it),it+(h/2)*ki1(t,st,it))

# kas para r
def kr1(t,st,it,rt):
    return r(t,st,it,rt)
    
def kr2(t,st,it,rt):
    return r(t+(h/2),st+(h/2)*ks1(t,st,it),it+(h/2)*ki1(t,st,it),rt+(h/2)*kr1(t,st,it,rt))


for t in ts[:-1]:
    us.append(us[-1]+h*ks2(t,us[-1],ui[-1]))
    ui.append(ui[-1]+h*ki2(t,us[-2],ui[-1]))
    ur.append(ur[-1]+h*kr2(t,us[-2],ui[-2],ur[-1]))
    
plt.plot(ts, us, 'b-', label = '$s(t)$')
plt.plot(ts, ui, 'r-', label = '$i(t)$')
plt.plot(ts, ur, 'g-', label = '$r(t)$')
plt.legend()
plt.xlabel('Days')
plt.grid()